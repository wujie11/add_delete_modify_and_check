create table Student
(
StudentId int primary key identity(1,1),
StudentName nvarchar(10) not null,
age int default 18,
score int default 0
)
go
insert into Student (StudentName) values ('李三')
insert into Student (StudentName) values ('李四')
insert into Student (StudentName) values ('小王')
insert into Student (StudentName) values ('王炸')
insert into Student (StudentName) values ('小四')
insert into Student (StudentName) values ('小屋')
insert into Student (StudentName) values ('张三')
insert into Student (StudentName) values ('王刚')
insert into Student (StudentName) values ('小菜')
insert into Student (StudentName) values ('小猪')