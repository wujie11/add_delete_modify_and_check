﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: 这行代码将数据加载到表“user1DataSet.Student”中。您可以根据需要移动或删除它。
            //this.studentTableAdapter.Fill(this.user1DataSet.Student);
            var abc = "select * from Student";
            var dt = DbHelper.GetDataTable(abc);
            dataGridView1.DataSource = dt;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.ReadOnly = true;
            dataGridView1.AllowUserToAddRows = false;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var name = textBox1.Text;
            var sql = string.Format("select * from Student where StudentName like '%{0}%'",name);
            var dt = DbHelper.GetDataTable(sql);
            dataGridView1.DataSource = dt;
        }
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            EditForm form = new EditForm();
            var res=form.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var abc = "select * from Student";
                var dt = DbHelper.GetDataTable(abc);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("no");
            }
        }
        /// <summary>
        /// 更改
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            var id =(int) dataGridView1.SelectedRows[0].Cells["studentIdDataGridViewTextBoxColumn"].Value;
            var name = (string)dataGridView1.SelectedRows[0].Cells["studentNameDataGridViewTextBoxColumn"].Value;
            var age = (int)dataGridView1.SelectedRows[0].Cells["ageDataGridViewTextBoxColumn"].Value;
            var score = (int)dataGridView1.SelectedRows[0].Cells["scoreDataGridViewTextBoxColumn"].Value;
            EditForm form = new EditForm(id,name,age,score);
            var res=form.ShowDialog();
            if (res == DialogResult.Yes)
            {
                var abc = "select * from Student";
                var dt = DbHelper.GetDataTable(abc);
                dataGridView1.DataSource = dt;
            }
            else
            {
                MessageBox.Show("no");
            }
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;
            if (MessageBox.Show("是否删除", "提示", MessageBoxButtons.YesNo) ==this.DialogResult)
            {
                var id = (int)dataGridView1.SelectedRows[0].Cells["studentIdDataGridViewTextBoxColumn"].Value;
                var sql = string.Format("delete  from Student where StudentId={0}", id);
                var res = DbHelper.AddOrUpdate(sql);
                if (res > 0)
                {
                    MessageBox.Show("删除成功", "提示");
                    var abc = "select * from Student";
                    var dt = DbHelper.GetDataTable(abc);
                    dataGridView1.DataSource = dt;
                }

            }
            else
            {
                MessageBox.Show("删除失败", "提示");
                var abc = "select * from Student";
                var dt = DbHelper.GetDataTable(abc);
                dataGridView1.DataSource = dt;
            }



        }
    }
}
